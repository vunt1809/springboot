package com.digidinos.shopping;

import java.util.Arrays;
import java.util.Date;
import java.util.stream.Collectors;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import com.digidinos.shopping.entity.Account;
import com.digidinos.shopping.entity.Product;
import com.digidinos.shopping.service.AccountService;
import com.digidinos.shopping.service.ProductService;
import com.github.javafaker.Faker;

@Component
public class MigrateDataCommandRunner implements CommandLineRunner{

	protected final Log logger = LogFactory.getLog(getClass());

	@Autowired
	private ProductService prodService;
	
	@Autowired
	private AccountService accountService;

	public void run(String... args) throws Exception {
		String strArgs = Arrays.stream(args).collect(Collectors.joining("|"));
		logger.info("Application started with arguments:" + strArgs);

		if (strArgs.equals("migrate")) {
			migratedDataAccount();
		}
	}

	protected void migratedData() {
		logger.info("Application started migratedData");

		for (int i = 0; i < 100; i++) {
			Product entity = new Product();
			Faker faker = new Faker();
			
			entity.setName(faker.company().name());
			entity.setDesception(faker.book().title());
			entity.setPrice((double) (Math.random() * 200 + 1));

			prodService.addReservation(entity);
		}
	}
	
	protected void migratedDataAccount() {
		logger.info("Application started migratedData");
		BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
		for (int i = 0; i < 100; i++) {
			Account entity = new Account();
			Faker faker = new Faker();	
			entity.setUserName(faker.leagueOfLegends().champion());
			entity.setDesception(faker.book().title());
			entity.setEncrytedPassword(passwordEncoder.encode("123"));
			entity.setActive(true);
			entity.setUserRole("ROLE_EMPLOYEE");
			entity.setCreatedAt(new Date());
			accountService.addReservation(entity);
		}
	}

}
