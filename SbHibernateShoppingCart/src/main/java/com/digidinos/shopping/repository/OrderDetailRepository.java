package com.digidinos.shopping.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.digidinos.shopping.entity.OrderDetail;

@Repository
public interface OrderDetailRepository extends BaseRepository<OrderDetail, Integer>{
	/**
	 * GET All OrderDetail by Id
	 * @param orderId Order_Id of OrderDetail
	 * @return List<OrderDetail> List of orderDetail
	 */
	@Query(value="SELECT * FROM Order_Details o where o.order_id = :orderId", nativeQuery = true) 
	List<OrderDetail> findAllByOrderId(Integer orderId);
	
}
