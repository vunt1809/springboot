package com.digidinos.shopping.service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.digidinos.shopping.entity.Product;
import com.digidinos.shopping.repository.ProductRepository;

@Service
public class ProductService {

	@Autowired
	private ProductRepository repo;

	/**
	 * Get top 5 new product
	 * 
	 * @return List of product
	 */	
	
	public List<Product> top5NewProduct(){
		List<Product> products = repo.top5ProductNew();
		return products;
	}

	/**
	 * Get top 1 new Product
	 * 
	 * @return Product
	 */
	public Product top1NewProduct(){
		Product product = repo.top1ProductNew();
		return product;
	}
	/**
	 * Get all Product
	 * 
	 * @return List<Product> List of product
	 */
	public List<Product> selectAll() {
		List<Product> products = repo.findAll();
		return products;
	}
	/**
	 * Get Page Product
	 * @param pageable
	 * @return 
	 */
	public Page<Product> findAll(Pageable pageable){
		return repo.findAll(pageable);
	}
	/**
	 * Get All Product by Name
	 * @param name Name of Product
	 * @return List<Product> List of product
	 */
	public List<Product> findByName(String name){
		return repo.findByName(name);
	}
	/**
	 * Get All Product by Name
	 * @param name Name of product
	 * @param pageable
	 * @return Page<Product> Page of product
	 */
	public Page<Product> findByNamePage(String name,Pageable pageable){
		return repo.findByNamePage(name,pageable);
	}

	/**
	 * Get Product by Id
	 * @param id Id of product
	 * @return Optional<Product>
	 */
	public Optional<Product> selectById(int id) {
		return repo.findById(id);
	}
	
	/**
	 * Get All Product by Price
	 * @param price Price of product
	 * @return List<Product> List of product
	 */
	public List<Product> selectByPrice(int price) {
		return repo.findByPrice(price);
	}

	/**
	 * Adding product entity into database
	 * @param product entity
	 */
	public void addReservation(Product product) {
		repo.save(product);
	}

	/**
	 * Updating product into database
	 * @param id Id of product
	 * @param product entity
	 */
	public void updateReservation(Product product) {
		Product product2 = new Product();
		Optional<Product> productOpt = selectById(product.getId());
		if(productOpt.isPresent()) {
			product2 = productOpt.get();
			product2.setUpdatedAt(new Date());
			product2.setName(product.getName());
			product2.setPrice(product.getPrice());
			product2.setImage(product.getImage());
		}
		repo.save(product2);
	}

	/**
	 * Soft delete product
	 * @param id Id of product
	 */
	public void deleteReservation(int id) {
		Optional<Product> productOpt = selectById(id);
		Product product = productOpt.get();
		product.setDelete(true);
		product.setDeletedAt(new Date());
		repo.save(product);
	}
}
