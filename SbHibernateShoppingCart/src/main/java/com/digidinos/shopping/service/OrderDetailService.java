package com.digidinos.shopping.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.digidinos.shopping.entity.OrderDetail;
import com.digidinos.shopping.entity.Product;
import com.digidinos.shopping.model.OrderDetailInfo;
import com.digidinos.shopping.repository.OrderDetailRepository;

@Service
public class OrderDetailService {
	
	@Autowired
	private OrderDetailRepository repo;
	
	public OrderDetailService() {
		// TODO Auto-generated constructor stub
	}
	/**
	 * Get OrderDetail by Id
	 * @param id Id of OrderDetail
	 * @return Optional<OrderDetail>
	 */
	public Optional<OrderDetail> selectById(int id) {
		Optional<OrderDetail> orderDetail = repo.findById(id);
		return orderDetail;
	}
	/**
	 * Get All OrderDetail by Order_ID
	 * @param orderId of OrderDetail
	 * @return List<OrderDetail> List of OrderDetail
	 */
	
	public List<OrderDetail> findAllByOrderId(Integer orderID){
		List<OrderDetail> listOrderDetail = repo.findAllByOrderId(orderID);
		return listOrderDetail;
	}
}
