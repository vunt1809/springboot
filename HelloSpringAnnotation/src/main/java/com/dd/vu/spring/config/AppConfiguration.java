package com.dd.vu.spring.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import com.dd.vu.spring.lang.Language;
import com.dd.vu.spring.lang.impl.Vietnamese;

@Configuration
@ComponentScan({"com.dd.vu.spring.bean"})
public class AppConfiguration {
	@Bean(name="language")
	public Language getLanguage() {
		return new Vietnamese();
	}
}
