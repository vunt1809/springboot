package com.digidinos.shopping.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.digidinos.shopping.entity.Account;
import com.digidinos.shopping.entity.BaseEntity;
import com.digidinos.shopping.entity.Product;

@Repository
public interface AccountRepository extends BaseRepository<Account, Integer> {
	
	/**
     * Get Account by userName
     * @param UserName of Account
     * @return Account entity 
     */
	Account findByUserName(String userName);
	
	
	/**
     * Get All Account by user_name
     * @param UserName of Account
     * @return Page<Acccount> Page of account  
     */
	@Query(value="Select a.* from Accounts a where a.user_name like %:name%",nativeQuery = true)
	Page<Account> findByUserName(@Param("name") String name,Pageable pageable);
	
	
}
