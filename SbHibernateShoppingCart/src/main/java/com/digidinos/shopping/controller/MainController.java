package com.digidinos.shopping.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.digidinos.shopping.entity.Account;
import com.digidinos.shopping.entity.Product;
import com.digidinos.shopping.form.CustomerForm;
import com.digidinos.shopping.model.CartInfo;
import com.digidinos.shopping.model.CustomerInfo;
import com.digidinos.shopping.model.ProductInfo;
import com.digidinos.shopping.pagination.Pagination;
import com.digidinos.shopping.service.AccountService;
import com.digidinos.shopping.service.OrderService;
import com.digidinos.shopping.service.ProductService;
import com.digidinos.shopping.utils.Utils;
import com.digidinos.shopping.validator.CustomerFormValidator;

import oracle.net.aso.p;

@Controller
@Transactional
public class MainController {

	@Autowired
	private ProductService productService;

	@Autowired
	private AccountService accountService;

	@Autowired
	private Pagination pagination;

	@Autowired
	private OrderService orderService;

	@Autowired
	private CustomerFormValidator customerFormValidator;

	@InitBinder
	public void myInitBinder(WebDataBinder dataBinder) {
		Object target = dataBinder.getTarget();
		if (target == null) {
			return;
		}
		System.out.println("Target=" + target);
		// Trường hợp update SL trên giỏ hàng.
		// (@ModelAttribute("cartForm") @Validated CartInfo cartForm)
		if (target.getClass() == CartInfo.class) {

		}
		// Trường hợp save thông tin khách hàng.
		// (@ModelAttribute @Validated CustomerInfo customerForm)
		else if (target.getClass() == CustomerForm.class) {
			dataBinder.setValidator(customerFormValidator);
		}

	}

	@RequestMapping(value= {"/*","/*/*"})
	public String accessDenied() {
		return "/403";
	}
	/**
	 * 
	 */
	@RequestMapping(value= {"/home","/"})
	public String home(Model model, @RequestParam(value = "name", required = false,defaultValue = "") String likeName,
			@RequestParam(value = "page", required = false, defaultValue = "1") int currentPage,
			@RequestParam(value = "size", required = false, defaultValue = "8") int size) {
		List<Product> top5Products = productService.top5NewProduct();
		model.addAttribute("top5Products", top5Products);
		Product top1Product = productService.top1NewProduct();
		System.out.println("Top 1 : "+top1Product.getName());
		model.addAttribute("top1Product", top1Product);
		List<String> pagingNumber = new ArrayList();
		if (!likeName.isEmpty()) {
			Pageable pageable = PageRequest.of(currentPage - 1, size);
			Page<Product> productPage = productService.findByNamePage(likeName, pageable);
			pagingNumber = pagination.paginationProduct(productPage, currentPage);
			System.out.println("TotalElement Like Na : "+productPage.getTotalElements());
			
			if(productPage.isEmpty()) {
				return "/403";
			}
			model.addAttribute("searchName", likeName);
			model.addAttribute("pagination", pagingNumber);
			model.addAttribute("paginationPages", productPage);
			return "/productList";
		}
		model.addAttribute("searchName", likeName);
		Pageable pageable = PageRequest.of(currentPage - 1, size);
		Page<Product> productPage = productService.findAll(pageable);
		System.out.println("TotalElement : "+productPage.getTotalElements());
		
		System.out.println("Index   : "+(productPage.getNumber()+1));
		System.out.println("index 1 + : "+(productPage.getNumber()+7));
		System.out.println("Index 8  : "+(productPage.getNumberOfElements()*(productPage.getNumber()+1)));
		System.out.println("Total Page  : "+productPage.getTotalPages());
		pagingNumber = pagination.paginationProduct(productPage, currentPage);
		model.addAttribute("pagination", pagingNumber);
		model.addAttribute("paginationPages", productPage);
		return "index";
	}
	/**
	 * 
	 */
	@RequestMapping(value= {"/productDetail"})
	public String productDetail(Model model ,  @RequestParam(value = "id", defaultValue = "") Integer id) {
		Optional<Product> productOpt = productService.selectById(id);
		if(productOpt.isPresent()) {
			Product product = productOpt.get();
			model.addAttribute("productForm",product);
		}
		return "/productDetail";
	}
	

	/**
	 * Hiển thị danh sách sản phẩm
	 */
	@RequestMapping({ "/productList" })
	public String listProductHandler(Model model,
			@RequestParam(value = "name", required = false, defaultValue = "") String likeName,
			@RequestParam(value = "page", required = false, defaultValue = "1") int currentPage,
			@RequestParam(value = "size", required = false, defaultValue = "8") int size) {
		List<String> pagingNumber = new ArrayList();
		if (!likeName.isEmpty()) {
			Pageable pageable = PageRequest.of(currentPage - 1, size);
			Page<Product> productPage = productService.findByNamePage(likeName, pageable);
			pagingNumber = pagination.paginationProduct(productPage, currentPage);
			if(productPage.isEmpty()) {
				return "/403";
			}
			model.addAttribute("searchName", likeName);
			model.addAttribute("pagination", pagingNumber);
			model.addAttribute("paginationPages", productPage);
			return "/productList";
		}
		Pageable pageable = PageRequest.of(currentPage - 1, size);
		Page<Product> productPage = productService.findAll(pageable);
		pagingNumber = pagination.paginationProduct(productPage, currentPage);
		model.addAttribute("searchName", likeName);
		model.addAttribute("pagination", pagingNumber);
		model.addAttribute("paginationPages", productPage);
		return "productList";
	}
	/**
	 * Bấm nút mua sản phẩm , thêm sản phẩm vào cart
	 */
	@RequestMapping({ "/buyProduct" })
	public String listProductHandler(HttpServletRequest request, Model model, //

			@RequestParam(value = "id", defaultValue = "") Integer id) {

		Product product = null;
		if (id != null) {
			Optional<Product> productOpt = productService.selectById(id);
			product = productOpt.get();
		}
		if (product != null) {
			CartInfo cartInfo = Utils.getCartInSession(request);
			ProductInfo productInfo = new ProductInfo(product);
			cartInfo.addProduct(productInfo, 1);
		}
		return "redirect:/shoppingCart";
	}
	/**
	 * Xóa sản phẩm trong cart
	 */
	@RequestMapping({ "/shoppingCartRemoveProduct" })
	public String removeProductHandler(HttpServletRequest request, Model model, //

			@RequestParam(value = "id", defaultValue = "") Integer id) {
		Product product = null;
		if (id != null) {
			Optional<Product> productOpt = productService.selectById(id);
			product = productOpt.get();
		}
		if (product != null) {
			CartInfo cartInfo = Utils.getCartInSession(request);
			ProductInfo productInfo = new ProductInfo(product);
			cartInfo.removeProduct(productInfo);
		}
		return "redirect:/shoppingCart";
	}

	/**
	 * Update số lượng sản phẩm trong giỏ hàng
	 */
	@RequestMapping(value = { "/shoppingCart" }, method = RequestMethod.POST)
	public String shoppingCartUpdateQty(HttpServletRequest request, //
			Model model, //
			@ModelAttribute("cartForm") CartInfo cartForm) {

		CartInfo cartInfo = Utils.getCartInSession(request);
		cartInfo.updateQuantity(cartForm);

		return "redirect:/shoppingCart";
	}

	/**
	 * Hiển thị thông tin của giỏ hàng
	 */
	@RequestMapping(value = { "/shoppingCart" }, method = RequestMethod.GET)
	public String shoppingCartHandler(HttpServletRequest request, Model model) {
		CartInfo myCart = Utils.getCartInSession(request);

		model.addAttribute("cartForm", myCart);
		return "shoppingCart";
	}

	/**
	 * Hiển thị trang thông tin của khách hàng
	 */
	@RequestMapping(value = { "/shoppingCartCustomer" }, method = RequestMethod.GET)
	public String shoppingCartCustomerForm(HttpServletRequest request, Model model) {

		CartInfo cartInfo = Utils.getCartInSession(request);

		if (cartInfo.isEmpty()) {

			return "redirect:/shoppingCart";
		}
		CustomerInfo customerInfo = cartInfo.getCustomerInfo();

		CustomerForm customerForm = new CustomerForm(customerInfo);

		model.addAttribute("customerForm", customerForm);

		return "shoppingCartCustomer";
	}

	/**
	 * Lưu thông tin khách hàng 
	 */
	@RequestMapping(value = { "/shoppingCartCustomer" }, method = RequestMethod.POST)
	public String shoppingCartCustomerSave(HttpServletRequest request, //
			Model model, //
			@ModelAttribute("customerForm") @Validated CustomerForm customerForm, //
			BindingResult result, //
			final RedirectAttributes redirectAttributes) {

		if (result.hasErrors()) {
			customerForm.setValid(false);
			// Forward tới trang nhập lại.
			return "shoppingCartCustomer";
		}

		customerForm.setValid(true);
		CartInfo cartInfo = Utils.getCartInSession(request);
		CustomerInfo customerInfo = new CustomerInfo(customerForm);
		cartInfo.setCustomerInfo(customerInfo);

		return "redirect:/shoppingCartConfirmation";
	}

	/**
	 * Xác nhận lại thông tin khách hàng và giỏ hàng
	 */
	@RequestMapping(value = { "/shoppingCartConfirmation" }, method = RequestMethod.GET)
	public String shoppingCartConfirmationReview(HttpServletRequest request, Model model) {
		CartInfo cartInfo = Utils.getCartInSession(request);

		if (cartInfo == null || cartInfo.isEmpty()) {

			return "redirect:/shoppingCart";
		} else if (!cartInfo.isValidCustomer()) {

			return "redirect:/shoppingCartCustomer";
		}
		model.addAttribute("myCart", cartInfo);

		return "shoppingCartConfirmation";
	}

	/**
	 * Gửi đơn hàng . Insert to database
	 */
	@RequestMapping(value = { "/shoppingCartConfirmation" }, method = RequestMethod.POST)

	public String shoppingCartConfirmationSave(HttpServletRequest request, Model model) {
		CartInfo cartInfo = Utils.getCartInSession(request);

		if (cartInfo.isEmpty()) {

			return "redirect:/shoppingCart";
		} else if (!cartInfo.isValidCustomer()) {

			return "redirect:/shoppingCartCustomer";
		}
		try {
			orderService.addOrder(cartInfo);
		} catch (Exception e) {

			return "shoppingCartConfirmation";
		}

		// Xóa giỏ hàng khỏi session.
		Utils.removeCartInSession(request);

		// Lưu thông tin đơn hàng cuối đã xác nhận mua.
		Utils.storeLastOrderedCartInSession(request, cartInfo);

		return "redirect:/shoppingCartFinalize";
	}
	/**
	 * Hiển thị trang cảm ơn khi mua hàng thành công
	 */
	@RequestMapping(value = { "/shoppingCartFinalize" }, method = RequestMethod.GET)
	public String shoppingCartFinalize(HttpServletRequest request, Model model) {

		CartInfo lastOrderedCart = Utils.getLastOrderedCartInSession(request);

		if (lastOrderedCart == null) {
			return "redirect:/shoppingCart";
		}
		model.addAttribute("lastOrderedCart", lastOrderedCart);
		return "shoppingCartFinalize";
	}
	/**
	 * Hiển thị ảnh của product
	 */
	@RequestMapping(value = { "/productImage" }, method = RequestMethod.GET)
	public void productImage(HttpServletRequest request, HttpServletResponse response, Model model,

			@RequestParam("id") Integer id) throws IOException {
		Product product = null;
		if (id != null) {
			Optional<Product> productOpt = productService.selectById(id);
			if (productOpt.isPresent()) {
				product = productOpt.get();
			}
		}
		if (product != null && product.getImage() != null) {
			response.setContentType("image/jpeg, image/jpg, image/png, image/gif");
			response.getOutputStream().write(product.getImage());
		}
		response.getOutputStream().close();
	}
	/**
	 * Hiển thị ảnh của account
	 */
	@RequestMapping(value = { "/accountImage" }, method = RequestMethod.GET)
	public void accountImage(HttpServletRequest request, HttpServletResponse response, Model model,

			@RequestParam("id") Integer id) throws IOException {
		Account account = null;
		if (id != null) {
			Optional<Account> accountOpt = accountService.selectById(id);
			if (accountOpt.isPresent()) {
				account = accountOpt.get();
			}
		}
		if (account != null && account.getImage() != null) {
			response.setContentType("image/jpeg, image/jpg, image/png, image/gif");
			response.getOutputStream().write(account.getImage());
		}
		response.getOutputStream().close();
	}

}
