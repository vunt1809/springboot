package com.digidinos.shopping.service;
import java.util.Date;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.digidinos.shopping.entity.Account;
import com.digidinos.shopping.entity.Product;
import com.digidinos.shopping.repository.AccountRepository;

@Service
public class AccountService {
	
	@Autowired
	private AccountRepository repo;
	
	public AccountService() {
		// TODO Auto-generated constructor stub
	}
	/**
	 *  GET All Account
	 *  @param pageable Pageable
	 *  @return Page<Account> Page of account
	 */
	public Page<Account> findAll(Pageable pageable){
		return repo.findAll(pageable);
	}
	/**
	 * GET Account By UserName
	 * @param string userName of Account
	 * @return Account
	 */
	
	public Account findByUserName(String userName) {
		Account account = repo.findByUserName(userName);
		return account;
	}
	/**
	 * GET All Account By Name
	 * @param name of Account
	 * @param pageable
	 * @return Page<Account> Page of account
	 */
	public Page<Account> findByUserName(String name,Pageable pageable){
		return repo.findByUserName(name,pageable);
	}
	/**
	 * Update Role Account in database
	 * @param account entity
	 */
	public void updateRole(Account account) {

        Optional<Account> accountOtp = repo.findById(account.getId());
        Account account2 = new Account();
        if (accountOtp.isPresent()) {
            account2 = accountOtp.get();
            account2.setUpdatedAt(new Date());
            account2.setUserRole(account.getUserRole());
        }
        this.repo.save(account2);
    }
	/**
	 *  Change Pasword Account in database
	 * @param account entity
	 */
	public void updatePassword(Account account) {

        Optional<Account> accountOtp = repo.findById(account.getId());
        Account account2 = new Account();
        if (accountOtp.isPresent()) {
            account2 = accountOtp.get();
            account2.setUpdatedAt(new Date());
            account2.setEncrytedPassword(account.getEncrytedPassword());
        }
        this.repo.save(account2);
    }
	/**
	 *  Update Profile Account
	 * @param account entity
	 */
	public void updateProfile(Account account) {
        Optional<Account> accountOtp = repo.findById(account.getId());
        Account account2 = new Account();
        if (accountOtp.isPresent()) {
            account2 = accountOtp.get();
            account2.setAddress(account.getAddress());
            account2.setDesception(account.getDesception());
            account2.setUpdatedAt(new Date());
            account2.setImage(account.getImage());
        }
        this.repo.save(account2);
    }
	/**
	 * Insert Account to database
	 * @param account
	 */
	public void addReservation(Account account) {
		repo.save(account);
	}
	/**
	 * GET Account by ID
	 * @param id Id of Account
	 * @return account entity
	 */
	public Optional<Account> selectById(int id){
		Optional<Account> accountOpt = repo.findById(id);
		return accountOpt;
	}
	/**
	 *  Soft Delete Account by ID
	 * @param id Id of Account
	 */
	public void deleteReservation(int id) {
		Optional<Account> accountOpt = repo.findById(id);
		Account account = accountOpt.get();
		account.setActive(false);
		account.setDeletedAt(new Date());
		repo.save(account);
	}
}
