package com.digidinos.shopping.form;

import java.io.IOException;
import java.util.Date;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.multipart.MultipartFile;

import com.digidinos.shopping.entity.Account;

public class AccountForm {
	private int id;
	private String userName;
	private String encrytedPassword;
	private String verifyPassword;
	private String userRole;
	private boolean active;
	private boolean newAccount = false;

	public AccountForm() {
		this.newAccount = true;
	}
	
	BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

	public AccountForm(Account account) {
		this.id = account.getId();
		this.userName = account.getUserName();
		this.encrytedPassword = account.getEncrytedPassword();
		this.userRole = account.getUserRole();
		this.active = account.isActive();
	}
	
	public Account getAccount() throws IOException {
		Account account = new Account();
		account.setId(this.id);
		account.setUserName(this.userName);
		if(this.encrytedPassword!=null) {
			account.setEncrytedPassword(passwordEncoder.encode(this.encrytedPassword));
		}
		account.setUserRole(this.userRole);
		account.setCreatedAt(new Date());
		account.setActive(true);
		return account;	
	}
	
	public String getVerifyPassword() {
		return verifyPassword;
	}

	public void setVerifyPassword(String verifyPassword) {
		this.verifyPassword = verifyPassword;
	}

	public boolean isNewAccount() {
		return newAccount;
	}

	public void setNewAccount(boolean newAccount) {
		this.newAccount = newAccount;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getEncrytedPassword() {
		return encrytedPassword;
	}

	public void setEncrytedPassword(String encrytedPassword) {
		this.encrytedPassword = encrytedPassword;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserRole() {
		return userRole;
	}

	public void setUserRole(String userRole) {
		this.userRole = userRole;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

}
