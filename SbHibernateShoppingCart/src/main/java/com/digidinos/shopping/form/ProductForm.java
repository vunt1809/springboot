package com.digidinos.shopping.form;

import java.io.IOException;
import java.util.Date;

import org.springframework.web.multipart.MultipartFile;

import com.digidinos.shopping.entity.Product;

import net.bytebuddy.asm.Advice.This;
 
public class ProductForm {
	private int id;
    private String name;
    private double price;
    private String desception;
    private boolean isDelete;
    private boolean newProduct = false;
    private byte[] image;
 
    // Upload file.
    private MultipartFile fileData;
 
    public ProductForm() {
        this.newProduct= true;
    }
 
    public ProductForm(Product product) {
    	this.id = product.getId();
        this.name = product.getName();
        this.price = product.getPrice();
        this.desception = product.getDesception();
        this.image = product.getImage();
    }
    
    public Product getProduct( ) throws IOException {
    	Product product = new Product();
    	product.setId(this.id);
    	product.setName(this.name);
    	product.setPrice(this.price);
    	product.setDesception(this.desception);
    	product.setDelete(false);  
    	product.setCreatedAt(new Date());
    	if(this.fileData.isEmpty()) {
    		product.setImage(this.image);
    	}else {
    		product.setImage(this.getFileData().getBytes());
    	}
    	return product;
    }
    
    
    
    public String getDesception() {
		return desception;
	}

	public void setDesception(String desception) {
		this.desception = desception;
	}

	public byte[] getImage() {
		return image;
	}

	public void setImage(byte[] image) {
		this.image = image;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public boolean isDelete() {
		return isDelete;
	}

	public void setDelete(boolean isDelete) {
		this.isDelete = isDelete;
	}
 
    public String getName() {
        return name;
    }
 
    public void setName(String name) {
        this.name = name;
    }
 
    public double getPrice() {
        return price;
    }
 
    public void setPrice(double price) {
        this.price = price;
    }
 
    public MultipartFile getFileData() {
        return fileData;
    }
 
    public void setFileData(MultipartFile fileData) {
        this.fileData = fileData;
    }
 
    public boolean isNewProduct() {
        return newProduct;
    }
 
    public void setNewProduct(boolean newProduct) {
        this.newProduct = newProduct;
    }
 
}
