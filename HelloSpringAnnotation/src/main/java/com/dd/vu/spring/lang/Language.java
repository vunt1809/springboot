package com.dd.vu.spring.lang;

public interface Language {
	public String getGreeting();
	public String getBye();
}
