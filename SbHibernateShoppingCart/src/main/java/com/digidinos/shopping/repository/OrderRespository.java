package com.digidinos.shopping.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.digidinos.shopping.entity.Order;
import com.digidinos.shopping.entity.Product;

@Repository
public interface OrderRespository extends BaseRepository<Order, Integer> {
	
	/**
	 * GET Int Max OrderNum
	 * @return int maxOrderNum
	 */
	@Query(value="select max(o.order_num) from Orders o", nativeQuery = true)
	int maxOrderNum();
	/**
	 * Get Order by Id
	 * @param id Id of Order
	 * return Order 
	 */
	@Query(value="SELECT * FROM Orders o where o.id = :orderId", nativeQuery = true) 
	Order findOrderInfoByID(int orderId);
	/**
	 * GET All Order by Customer_Name
	 * @param name Customer_Name of Order
	 * return Page<Order> Page of order
	 */
	@Query(value="Select * from Orders o where o.Customer_Name like %:name%",nativeQuery = true)
	Page<Order> findByCustomerName(@Param("name") String name,Pageable pageable);
}
