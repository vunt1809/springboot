package com.digidinos.shopping.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.digidinos.shopping.entity.Account;
import com.digidinos.shopping.entity.Order;
import com.digidinos.shopping.entity.OrderDetail;
import com.digidinos.shopping.entity.Product;
import com.digidinos.shopping.form.AccountChangePasswordForm;
import com.digidinos.shopping.form.AccountForm;
import com.digidinos.shopping.form.AccountInfoForm;
import com.digidinos.shopping.form.AccountUpdateRoleForm;
import com.digidinos.shopping.form.ProductForm;
import com.digidinos.shopping.model.OrderDetailInfo;
import com.digidinos.shopping.model.OrderInfo;
import com.digidinos.shopping.pagination.Pagination;
import com.digidinos.shopping.service.AccountService;
import com.digidinos.shopping.service.OrderDetailService;
import com.digidinos.shopping.service.OrderService;
import com.digidinos.shopping.service.ProductService;
import com.digidinos.shopping.validator.AccountChangePasswordValidator;
import com.digidinos.shopping.validator.AccountFormValidator;
import com.digidinos.shopping.validator.AccountInfoValidator;
import com.digidinos.shopping.validator.ProductFormValidator;

@Controller
public class AdminController {

	private static final Validator AccountChangePasswordValidator = null;

	@Autowired
	private AccountService accountService;

	@Autowired
	private ProductService productService;

	@Autowired
	private OrderService orderService;

	@Autowired
	private OrderDetailService orderDetailService;

	@Autowired
	private ProductFormValidator productFormValidator;

	@Autowired
	private AccountFormValidator accountFormValidator;

	@Autowired
	private AccountInfoValidator accountInfoValidator;

	@Autowired
	private AccountChangePasswordValidator accountChanePasswordValidator;

	@Autowired
	private Pagination pagination;

	@InitBinder
	public void myInitBinder(WebDataBinder dataBinder) {
		Object target = dataBinder.getTarget();
		if (target == null) {
			return;
		}
		System.out.println("Target=" + target);

		if (target.getClass() == ProductForm.class) {
			dataBinder.setValidator(productFormValidator);
			System.out.println("Product Valid");
		}
		if (target.getClass() == AccountForm.class) {
			dataBinder.setValidator(accountFormValidator);
			System.out.println("Account Valid");
		}
		if (target.getClass() == AccountChangePasswordForm.class) {
			dataBinder.setValidator(accountChanePasswordValidator);
			System.out.println("Account Change Password Valid");
		}
		if (target.getClass() == AccountInfoForm.class) {
			dataBinder.setValidator(accountInfoValidator);
			System.out.println("Account Info Validator Valid");
		}
	}

	/**
	 * Hiển thị trang login
	 */
	@RequestMapping(value = { "/admin/login" }, method = RequestMethod.GET)
	public String login(Model model) {
		return "login";
	}

	/**
	 * Hiển thị trang thông tin cá nhân
	 */
	@RequestMapping(value = { "/admin/accountInfo" }, method = RequestMethod.GET)
	public String userInfo(Model model) {
		UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		AccountInfoForm accountInfoForm = null;
		Account account = accountService.findByUserName(userDetails.getUsername());
		accountInfoForm = new AccountInfoForm(account);
		model.addAttribute("accountInfoForm", accountInfoForm);
		return "back-end/accountInfo";
	}

	/**
	 * Cập nhập thông tin cá nhân . 
	 */
	@RequestMapping(value = { "/admin/accountInfo" }, method = RequestMethod.POST)
	public String usertUpdate(Model model,
			@ModelAttribute("accountInfoForm") @Validated AccountInfoForm accountInfoForm, BindingResult result,
			final RedirectAttributes redirectAttributes) {
		if (result.hasErrors()) {
			return "back-end/accountInfo";
		}
		try {
			accountService.updateProfile(accountInfoForm.getAccount());
		} catch (Exception e) {
			Throwable rootCause = ExceptionUtils.getRootCause(e);
			String message = rootCause.getMessage();
			model.addAttribute("errorMessage", message);
			return "back-end/accountInfo";
		}
		return "redirect:accountInfo";
	}
	/**
	 * Hiển thị trang thêm mới account
	 */
	@RequestMapping(value = { "/admin/accountInsert" }, method = RequestMethod.GET)
	public String accountInsert(Model model, @RequestParam(value = "id", defaultValue = "") Integer id) {
		AccountForm accountForm = null;

		if (id != null) {
			Optional<Account> accountOpt = accountService.selectById(id);
			if (accountOpt.isPresent()) {
				Account account = accountOpt.get();
				accountForm = new AccountForm(account);
			}
		}
		if (accountForm == null) {
			accountForm = new AccountForm();
			accountForm.setNewAccount(true);
		}
		model.addAttribute("accountForm", accountForm);
		return "back-end/accountInsert";
	}
	/**
	 * Thêm mới account to database
	 */
	@RequestMapping(value = { "/admin/accountInsert" }, method = RequestMethod.POST)
	public String accountSave(Model model, @ModelAttribute("accountForm") @Validated AccountForm accountForm,
			BindingResult result, final RedirectAttributes redirectAttributes) {
		if (result.hasErrors()) {
			System.out.println("RESSULTTT");
			System.out.println(result.getAllErrors());
			return "back-end/accountInsert";
		}
		try {
			System.out.println("ADDD FINal");
			accountService.addReservation(accountForm.getAccount());
		} catch (Exception e) {
			System.out.println("CATHHH");
			Throwable rootCause = ExceptionUtils.getRootCause(e);
			String message = rootCause.getMessage();
			model.addAttribute("errorMessage", message);
			return "back-end/accountInsert";
		}
		return "redirect:accountManager";
	}
	/**
	 * Hiển thị trang sửa role của account
	 */
	@RequestMapping(value = { "/admin/accountUpdate" }, method = RequestMethod.GET)
	public String accountUpdate(Model model, @RequestParam(value = "id", defaultValue = "") Integer id) {
		AccountUpdateRoleForm accountUpdateRoleForm = null;
		if (id != null) {
			Optional<Account> accountOpt = accountService.selectById(id);
			if (accountOpt.isPresent()) {
				Account account = accountOpt.get();
				accountUpdateRoleForm = new AccountUpdateRoleForm(account);
			}
		}
		model.addAttribute("accountUpdateRoleForm", accountUpdateRoleForm);
		return "back-end/accountUpdate";
	}
	/**
	 * Chỉnh sửa role Account
	 */
	@RequestMapping(value = { "/admin/accountUpdate" }, method = RequestMethod.POST)
	public String accountUpdate(Model model,
			@ModelAttribute("AccountUpdateRoleForm") @Validated AccountUpdateRoleForm accountUpdateRoleForm,
			BindingResult result, final RedirectAttributes redirectAttributes) {
		if (result.hasErrors()) {
			System.out.println("RESULTTT");
			return "back-end/accountUpdate";
		}
		try {
			System.out.println("ACCOUNTT");
			accountService.updateRole(accountUpdateRoleForm.getAccount());
		} catch (Exception e) {
			System.out.println("ALTERR");
			Throwable rootCause = ExceptionUtils.getRootCause(e);
			String message = rootCause.getMessage();
			model.addAttribute("errorMessage", message);
			return "back-end/accountUpdate";
		}
		return "redirect:accountManager";
	}
	/**
	 * Hiển thị trang change password account
	 */
	@RequestMapping(value = { "/admin/accountChangePassword" }, method = RequestMethod.GET)
	public String userChangePass(Model model) {
		UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		AccountChangePasswordForm accountChangeForm = null;
		Account account = accountService.findByUserName(userDetails.getUsername());
		accountChangeForm = new AccountChangePasswordForm(account);
		accountChangeForm.setNewPassword(true);
		model.addAttribute("accountChangePasswordForm", accountChangeForm);
		return "back-end/accountChangePassword";
	}
	/**
	 * Update new password account
	 */
	@RequestMapping(value = { "/admin/accountChangePassword" }, method = RequestMethod.POST)
	public String usertChangePass(Model model,
			@ModelAttribute("accountChangePasswordForm") @Validated AccountChangePasswordForm accountChangePasswordForm,
			BindingResult result, final RedirectAttributes redirectAttributes) {
		if (result.hasErrors()) {
			System.out.println("RESULTTT");
			System.out.println(result.getAllErrors());
			return "back-end/accountChangePassword";
		}
		try {
			System.out.println("SUCESSSS");
			accountService.updatePassword(accountChangePasswordForm.getAccount());
		} catch (Exception e) {
			System.out.println("ALTER");
			Throwable rootCause = ExceptionUtils.getRootCause(e);
			String message = rootCause.getMessage();
			model.addAttribute("errorMessage", message);
			return "back-end/accountChangePassword";
		}
		return "redirect:accountInfo";
	}
	/**
	 *  Soft delete account
	 */
	@RequestMapping(value = { "admin/deleteAccount" }, method = RequestMethod.GET)
	public String removeAccount(Model model, //
			@RequestParam(value = "id", defaultValue = "") Integer id) {
		if (id != null) {
			accountService.deleteReservation(id);
		}
		return "redirect:accountManager";
	}

	/**
	 * Hiển thị tất cả các account
	 */
	@RequestMapping(value = { "/admin/accountManager" }, method = RequestMethod.GET)
	public String listUserHandler(Model model,
			@RequestParam(value = "name", required = false,defaultValue = "") String likeName,
			@RequestParam(value = "page", required = false, defaultValue = "1") int currentPage,
			@RequestParam(value = "size", required = false, defaultValue = "8") int size) {
		List<String> pagingNumber = new ArrayList();
		if (!likeName.isEmpty()) {
			Pageable pageable = PageRequest.of(currentPage - 1, size);
			Page<Account> pageAccount = accountService.findByUserName(likeName, pageable);
			pagingNumber = pagination.paginationAccount(pageAccount, currentPage);
			model.addAttribute("searchName", likeName);
			model.addAttribute("pagination", pagingNumber);
			model.addAttribute("paginationPages", pageAccount);
			return "back-end/accountManager";
		}
		Pageable pageable = PageRequest.of(currentPage-1, size);
		Page<Account> pageAccount = accountService.findAll(pageable);
		pagingNumber = pagination.paginationAccount(pageAccount, currentPage);
		model.addAttribute("searchName", likeName);
		model.addAttribute("pagination", pagingNumber);
		model.addAttribute("paginationPages", pageAccount);
		return "back-end/accountManager";
	}
	/**
	 * Hiển thị tất cả các product
	 */
	@RequestMapping(value = { "/admin/productManager" }, method = RequestMethod.GET)
	public String listProductHandler(Model model,
			@RequestParam(value = "name", required = false, defaultValue = "") String likeName,
			@RequestParam(value = "page", required = false, defaultValue = "1") int currentPage,
			@RequestParam(value = "size", required = false, defaultValue = "8") int size) {
		List<String> pagingNumber = new ArrayList();
//		Sort sortable = Sort.by("id").descending();
		if (!likeName.isEmpty()) {
			Pageable pageable = PageRequest.of(currentPage - 1, size);
			Page<Product> pageProduct = productService.findByNamePage(likeName, pageable);
			pagingNumber = pagination.paginationProduct(pageProduct, currentPage);
			model.addAttribute("searchName", likeName);
			model.addAttribute("pagination", pagingNumber);
			model.addAttribute("paginationPages", pageProduct);
			return "back-end/productManager";
		}
		Pageable pageable = PageRequest.of(currentPage - 1, size);
		Page<Product> productPage = productService.findAll(pageable);
		pagingNumber = pagination.paginationProduct(productPage, currentPage);
		model.addAttribute("searchName", likeName);
		model.addAttribute("pagination", pagingNumber);
		model.addAttribute("paginationPages", productPage);
		return "back-end/productManager";
	}

	// GET: Hiển thị product
	@RequestMapping(value = { "/admin/product" }, method = RequestMethod.GET)
	public String product(Model model, @RequestParam(value = "id", defaultValue = "") Integer id) {
		ProductForm productForm = null;

		if (id != null) {
			Optional<Product> productOpt = productService.selectById(id);
			if (productOpt.isPresent()) {
				Product product = productOpt.get();
				productForm = new ProductForm(product);
			}
		}
		if (productForm == null) {
			productForm = new ProductForm();
			productForm.setNewProduct(true);
		}
		model.addAttribute("productForm", productForm);
		return "back-end/productForm";
	}

	// POST: Save product
	@RequestMapping(value = { "/admin/product" }, method = RequestMethod.POST)
	public String productSave(Model model, @ModelAttribute("productForm") @Validated ProductForm productForm,
			BindingResult result, final RedirectAttributes redirectAttributes) {
		if (result.hasErrors()) {
			return "back-end/productForm";
		}
		try {
			if(productForm.getFileData().isEmpty()) {
				productService.updateReservation(productForm.getProduct());
			}
			productService.addReservation(productForm.getProduct());
		} catch (Exception e) {
			Throwable rootCause = ExceptionUtils.getRootCause(e);
			String message = rootCause.getMessage();
			model.addAttribute("errorMessage", message);
			return "back-end/productForm";
		}
		return "redirect:productManager";
	}

	// GET : Delete Product
	@RequestMapping(value = { "admin/deleteProduct" }, method = RequestMethod.GET)
	public String removeProductHandler(Model model, //
			@RequestParam(value = "id", defaultValue = "") Integer id) {
		if (id != null) {
			productService.deleteReservation(id);
		}
		return "redirect:productManager";
	}

	// GET : Hiển thị Order List
	@RequestMapping(value = { "/admin/orderManager" }, method = RequestMethod.GET)
	public String orderList(Model model,
			@RequestParam(value = "name", required = false,defaultValue = "") String likeName,
			@RequestParam(value = "page", required = false, defaultValue = "1") int currentPage,
			@RequestParam(value = "size", required = false, defaultValue = "8") int size) {
		List<String> pagingNumber = new ArrayList();
//		Sort sortable = Sort.by("id").descending();
		if (!likeName.isEmpty()) {
			Pageable pageable = PageRequest.of(currentPage - 1, size);
			Page<Order> pageProduct = orderService.findByCustomerName(likeName, pageable);
			pagingNumber = pagination.paginationOrder(pageProduct, currentPage);
			model.addAttribute("searchName",likeName);
			model.addAttribute("pagination", pagingNumber);
			model.addAttribute("paginationPages", pageProduct);
			return "back-end/orderManager";
		}
		Pageable pageable = PageRequest.of(currentPage-1, size);
		Page<Order> pageOrder = orderService.findAll(pageable);
		pagingNumber = pagination.paginationOrder(pageOrder, currentPage);
		model.addAttribute("searchName",likeName);
		model.addAttribute("pagination", pagingNumber);
		model.addAttribute("paginationPages", pageOrder);
		return "back-end/orderManager";
	}

	// GET : Hiển thị Order Detail
	@RequestMapping(value = { "/admin/order" }, method = RequestMethod.GET)
	public String orderView(Model model, @RequestParam("orderId") Integer orderId) {
		System.out.println("IDDD" + orderId);
		OrderInfo orderInfo = null;
		if (orderId != null) {
			Optional<Order> orderOpt = orderService.findById(orderId);
			if (orderOpt.isPresent()) {
				Order order = orderOpt.get();
				orderInfo = new OrderInfo(order.getId(), order.getOrderDate(), order.getOrderNum(), order.getAmount(),
						order.getCustomerName(), order.getCustomerAddress(), order.getCustomerEmail(),
						order.getCustomerPhone());
			}
			System.out.println("Order INFOO : " + orderInfo.getOrderNum());
		}
		if (orderInfo == null) {
			return "redirect:/admin/orderList";
		}
		List<OrderDetailInfo> listOrderDetailInfo = new ArrayList<>();
		List<OrderDetail> listOrderDetails = orderDetailService.findAllByOrderId(orderId);
		for (OrderDetail orderDetail : listOrderDetails) {
			OrderDetailInfo orderDetailInfo = new OrderDetailInfo(orderDetail.getId(), orderDetail.getProduct().getId(),
					orderDetail.getProduct().getName(), orderDetail.getQuanity(), orderDetail.getPrice(),
					orderDetail.getAmount());
			listOrderDetailInfo.add(orderDetailInfo);
		}
		orderInfo.setDetails(listOrderDetailInfo);

		System.out.println("Order INFOO Sau : " + orderInfo.getOrderNum());

		model.addAttribute("orderInfo", orderInfo);

		return "back-end/orderDetail";
	}
}
