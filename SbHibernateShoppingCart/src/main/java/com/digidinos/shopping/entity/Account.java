package com.digidinos.shopping.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Where;

@Entity
@DynamicUpdate
@Table(name = "Accounts")
@Where(clause = "Active=true")
public class Account extends BaseEntity implements Serializable {

	private static final long serialVersionUID = 1L;
	public static final String ROLE_MANAGER = "MANAGER";
	public static final String ROLE_EMPLOYEE = "EMPLOYEE";

	@Column(name = "User_Name", length = 20, nullable = false)
	private String userName;

	@Column(name = "Encryted_Password", length = 128, nullable = false)
	private String encrytedPassword;

	@Column(name = "Active", length = 1, nullable = false)
	private boolean active;

	@Column(name = "User_Role", length = 20, nullable = false)
	private String userRole;
	
	@Column(name="address",length =  Integer.MAX_VALUE,nullable = false)
	private String address;
	
	@Column(name="desception",length =  Integer.MAX_VALUE,nullable = false)
	private String desception;

	@Column(name = "Image", length = Integer.MAX_VALUE, nullable = true)
	private byte[] image;

	public String getUserName() {
		return userName;
	}
	
	public byte[] getImage() {
		return image;
	}

	

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getDesception() {
		return desception;
	}

	public void setDesception(String desception) {
		this.desception = desception;
	}

	public void setImage(byte[] image) {
		this.image = image;
	}



	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getEncrytedPassword() {
		return encrytedPassword;
	}

	public void setEncrytedPassword(String encrytedPassword) {
		this.encrytedPassword = encrytedPassword;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public String getUserRole() {
		return userRole;
	}

	public void setUserRole(String userRole) {
		this.userRole = userRole;
	}

	@Override
	public String toString() {
		return "[" + this.userName + "," + this.encrytedPassword + "," + this.userRole + "]";
	}

}