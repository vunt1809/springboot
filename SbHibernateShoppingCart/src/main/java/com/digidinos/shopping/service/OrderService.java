package com.digidinos.shopping.service;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.aspectj.weaver.ast.Or;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.digidinos.shopping.entity.Order;
import com.digidinos.shopping.entity.OrderDetail;
import com.digidinos.shopping.entity.Product;
import com.digidinos.shopping.form.CustomerForm;
import com.digidinos.shopping.model.CartInfo;
import com.digidinos.shopping.model.CartLineInfo;
import com.digidinos.shopping.model.CustomerInfo;
import com.digidinos.shopping.model.OrderInfo;
import com.digidinos.shopping.repository.OrderDetailRepository;
import com.digidinos.shopping.repository.OrderRespository;
import com.digidinos.shopping.repository.ProductRepository;

@Service
public class OrderService {
	@Autowired
	private OrderRespository repositoryOrder;

	@Autowired
	private ProductRepository repositoryProduct;

	@Autowired
	private OrderDetailRepository repositoryOrderDetail;

	public List<Order> findAll() {
		List<Order> orders = repositoryOrder.findAll();
		return orders;
	}
	/**
	 * GET Page Order by Customer_Name
	 * @param customer_name of Order
	 * @param pageable
	 * @return Page<Order> Page of order
	 */
	public Page<Order> findByCustomerName(String name,Pageable pageable){
		return repositoryOrder.findByCustomerName(name, pageable);
	}
	/**
	 * GET All Page Order
	 * @param pageable
	 * @return Page<Order> Page of oder
	 */
	public Page<Order> findAll(Pageable pageable){
		return repositoryOrder.findAll(pageable);
	}
	/**
	 * GET Page Order by Id
	 * @param id Id of Order
	 * @return	Optional<Order>
	 */
	public Optional<Order> findById(int id) {
		Optional<Order> order = repositoryOrder.findById(id);
		return order;
	}
	/**
	 * Get all Order by Id
	 * @param id Id of Order
	 * @return Page<Order> Page of order
	 */
	public List<Order> findAllById(Iterable<Integer> id) {
		List<Order> order = repositoryOrder.findAllById(id);
		return order;
	}
	
	/**
	 * Save order and orderDetail
	 * @param cartInfo
	 */
	public void addOrder(CartInfo cartInfo) {
		int orderNum = repositoryOrder.maxOrderNum() + 1 ;
		System.out.println(orderNum);
		Order order = new Order();
        order.setOrderNum(orderNum);
		order.setOrderDate(new Date());
		order.setAmount(cartInfo.getAmountTotal());
		
		CustomerInfo customerInfo = cartInfo.getCustomerInfo();
		order.setCustomerName(customerInfo.getName());
		order.setCustomerEmail(customerInfo.getEmail());
		order.setCustomerPhone(customerInfo.getPhone());
		order.setCustomerAddress(customerInfo.getAddress());
		System.out.println("Name : "+order.getCustomerName());

		repositoryOrder.save(order);

		List<CartLineInfo> lines = cartInfo.getCartLines();

		for (CartLineInfo line : lines) {
			OrderDetail detail = new OrderDetail();
			detail.setOrder(order);
			detail.setAmount(line.getAmount());
			detail.setPrice(line.getProductInfo().getPrice());
			detail.setQuanity(line.getQuantity());
			int id = line.getProductInfo().getId();
			Optional<Product> productOpt = repositoryProduct.findById(id);
			Product product = productOpt.get();
			detail.setProduct(product);
			repositoryOrderDetail.save(detail);
		}
	}

}
