package com.digidinos.shopping.repository;



import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.digidinos.shopping.entity.Account;
import com.digidinos.shopping.entity.Product;

@Repository
public interface ProductRepository extends BaseRepository<Product, Integer> {
	
	/**
	 *  GET All Product by Price
	 * @param price Price of Product
	 * @return List<Product> List of product
	 */
	@Query(value="SELECT t.* FROM products t where t.price = :price", nativeQuery = true) 
	List<Product> findByPrice(@Param("price") int price);
	/**
	 *  GET TOP 5 Product By Created_At DESC
	 *  @return List<Product> List of product
	 */
	@Query(value="select top 5 products.id , products.image,products.desception, products.created_at , products.deleted_at , products.updated_at , products.is_delete , products.name , products.price from products where products.is_delete = 0 order by products.created_at desc", nativeQuery = true) 
	List<Product> top5ProductNew();
	/**
	 * GET TOP 1 Product By PRICE DESC
	 * @return Product entity
	 */
	@Query(value="select top 1 products.id ,products.image,products.desception, products.created_at , products.deleted_at , products.updated_at, products.is_delete , products.name , products.price from products where products.is_delete = 0 order by products.price desc", nativeQuery = true) 
	Product top1ProductNew();
	/**
	 * GET List Product By Name and Is_delete = 0
	 * @param name Name of Product
	 * @return List<Product> List of product
	 */
	@Query(value="Select * from Products p where p.name like %:name% and is_delete = 0",nativeQuery = true)
	List<Product> findByName(@Param("name") String name);
	/**
	 * GET Page Product by Name and Is_delete = 0
	 * @param name Name of Product
	 * @param pageable Pageable
	 * @return List<Product> List of product
	 */
	@Query(value="Select * from Products p where p.name like %:name% and is_delete = 0",nativeQuery = true)
	Page<Product> findByNamePage(@Param("name") String name,Pageable pageable);
	/**
	 * GET All Product
	 * @param pagable
	 * @return Page<Product> Page of product
	 */
	@Query(value="Select * from Products where is_delete=0",nativeQuery = true)
	Page<Product> findAll(Pageable pageable);
	
	
	
}
