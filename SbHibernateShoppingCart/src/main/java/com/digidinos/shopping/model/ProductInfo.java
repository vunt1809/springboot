package com.digidinos.shopping.model;

import com.digidinos.shopping.entity.Product;

public class ProductInfo {
	private int id;
	private String name;
	private String desception;
	private double price;
	public boolean isDelete;

	public ProductInfo() {
	}

	public ProductInfo(Product product) {
		this.id = product.getId();
		this.name = product.getName();
		this.price = product.getPrice();
		this.desception = product.getDesception();
		this.isDelete = product.isDelete();
	}

	// Sử dụng trong JPA/Hibernate query
	public ProductInfo(int id, String name, double price,String desception,boolean isDelete) {
		this.id = id;
		this.name = name;
		this.desception=desception;
		this.price = price;
		this.isDelete = isDelete;
	}
	
	

	public String getDesception() {
		return desception;
	}

	public void setDesception(String desception) {
		this.desception = desception;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public boolean isDelete() {
		return isDelete;
	}

	public void setDelete(boolean isDelete) {
		this.isDelete = isDelete;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

}
